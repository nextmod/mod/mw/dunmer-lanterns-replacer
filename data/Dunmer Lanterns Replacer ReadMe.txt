=============================
 Dunmer Lanterns Replacer
=============================
by Melchior Dahrk
Version 12.0


=====================
 Description
=====================
This mod replaces all of the Dunmer lanterns (including the paper lanterns and streetlight) from the original game with smoother, more detailed versions (yet not with a ridiculous amount of new polies: they're optimized).

Breakdown of what's included:

Light_Ashl_Lantern_01/02/03/04/05/06/07
Light_De_Lantern_01/02/03/04/05/06/07/08/09/10/11/12/13/14
Light_De_streetlight_01
Light_paper_lantern_01/02/off


=====================
 Installation
=====================

Wrye Mash
Installation
==============
Archive is set up to use Sub-Packages for Mash Installers:

	## Pictures - Not required for installation. Refer to these pictures for details on the optional content.

	00 Core No Glow - includes the required files for this mod to work

	Optional

		01 Ashlander Lanterns Retexture no.1 - Retexture by Pherim. See pictures.
		01 Ashlander Lanterns Retexture no.2 - Retexture by SWG. See pictures.

		02 Ashlander Lantern (Smoothed Only) - Install if you just want a smoothed version of the ashlander lantern models without the extra details. See pictures.

		03 Glow Effect - Adds a glow effect around the lanterns

	Patches

		04 Tamriel Rebuilt Patch - Replaces the lanterns in TR to match the high quality models from this mod. 


Manual
Installation
==============
To install copy all files into the Data Files directory. No plugin to activate, you're ready to play!


=====================
 Permissions
=====================
You are free to:
Adapt - remix, transform, and build upon the material

Under the following terms:
Attribution - You must give appropriate credit.
Non-Commercial - You may not use the material for commercial purposes (i.e. no payment of monetary compensation as a condition for the download).

If author(s) is/are not able to be contacted through a reasonable effort,
Then you are free to copy and redistribute the material for use in The Elder Scrolls III: Morrowind


=====================
 Credits
=====================
Wolli - Made the TR patch
Pherim - For the Ashlander Lantern retexture no.1
starwarsgal9875 - For the Ashlander Lantern retexture no.2


=====================
 Changelog
=====================
1.0-5.0: I don't remember

6.0: All Ashlander bug lanterns have been replaced

7.0: Added Light_De_Lantern_01, 07, 11; fixed Ashlander orientation

8.0: Added Light_De_Lantern_05, 10, 14; fixed candle flame visibility in glass lanterns (thanks papill6n); fixed mesh opening on Light_De_Lantern_01, 07, 11

9.0: Removed collision from all lanterns (except the Streetlight which had collision before), fixed bounding box issues which were causing floaters when placing lanterns, used papill6n's candle flame visibility tweak on Light_De_Lantern_05, 10, 14 so that the flame should display properly

10.0: Optimization patch for all models to improve performance, with no loss in quality. Also fixed light source position, shadowbox, and bone offset (for held position) on some lanterns.
10.1: Fixed misplaced lanterns.
10.2: Visual improvement for light_de_lantern_02 (added wick and fixed flickering candle bug); repackaged file to be MASH compatible.

11.0: Fixed the optional smoothed ashlander lantern patch. Removed legacy support for lantern_04 alternates. Added TR patch, courtesy of Wolli. Revised Permissions.

12.0: Added optional glow effect to lanterns. Minor optimizations and improvements to a many models.