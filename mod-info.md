# Name
Dunmer Lanterns Replacer

# Created by
Melchior Dahrk

# Category
Models and Textures

# Description
This mod replaces all of the Dunmer lanterns (including the paper lanterns and streetlight) from the original game with smoother, more detailed versions (yet not with a ridiculous amount of new polies: they're optimized).

# Tags
* Lore-Friendly
* Replacer
* Models/Meshes
* Lighting
* Items - Clutter

# Release date
2014-08-07 15:32

# Last update date
2020-03-23 00:34

# Version
12.0

# Changelog
## Version 12.0
* Added optional glow effect to lanterns
* Minor optimizations and improvements to many models

## Version 11.0
* Fixed the optional smoothed ashlander lantern patch
* Removed legacy support for lantern_04 alternates
* Added TR patch, courtesy of Wolli
* Revised Permissions

## Version 10.2
* Visual improvement for light_de_lantern_02 (added wick and fixed flickering candle bug)
* Repackaged file to be MASH compatible.
* Removed thumbs.db to reduce file size

## Version 10.1
* Fixed misplaced lanterns

## Version 10.0
* Optimization patch for all models to improve performance, with no loss in quality
* Also fixed light source position, shadowbox, and bone offset (for held position) on some lanterns

## Version 9.0
* Removed collision from all lanterns (except the Streetlight which had collision before)
* Fixed bounding box issues which were causing floaters when placing lanterns
* Used papill6n's candle flame visibility tweak on Light_De_Lantern_05, 10, 14 so that the flame should display properly

## Version 8.0
* Added Light_De_Lantern_05, 10, 14
* Fixed candle flame visibility in glass lanterns (thanks papill6n)
* Fixed mesh opening on Light_De_Lantern_01, 07, 11

## Version 7.0
* Added Light_De_Lantern_01, 07, 11
* Fixed Ashlander lantern orientation

## Version 6.0
* All Ashlander bug lanterns have been replaced

## Version 1.0-5.0
* Initial release and updates.

# Credits
* Bethesda
* Blender Foundation
* Pherim (for the Ashlander Lantern retexture no.1, hosted here with permission)
* starwarsgal9875 (for the Ashlander Lantern retexture no.2, hosted here with permission)
* Wolli for the TR patch

# Permissions
You are free to:
Adapt - remix, transform, and build upon the material

## Under the following terms:
* Attribution - You must give appropriate credit.
* Non-Commercial - You may not use the material for commercial purposes (i.e. no payment of monetary compensation as a condition for the download).

If author(s) is/are not able to be contacted through a reasonable effort,
Then you are free to copy and redistribute the material for use in The Elder Scrolls III: Morrowind
